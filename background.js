//controls chrome actions and handle url changes

/////////////////////////////
////////should only run once
/////////////////////////////
    var settings;
    
    chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse)
    {
        switch(request.task)
        {
            /**
            *getting comand to start initEvents
            */
            case "initEvents":
            StartSurveys(); //start listening of redirect and new window 
            return true;
            break;

            case "settings":
            settings = request.settings;
            return true;
            break;
            
            case "startOver":
            OpenNextSurvey();
            break;
                
            case "removeLeftovers":
            RemoveLeftOvers();
            break;

        }
    });
    //////////////////////////////////////////
    ///sending task to dom for starting surveys
    //////////////////////////////////////////
var surveyTab;
    function StartSurveys()
    {
        //// send response to click the survey as event are initialised
        setTimeout(function(){
            chrome.tabs.query({active:true},function(send){
                chrome.tabs.sendMessage(send[0].id,{task:"openSurvey"});
            });           
        },50);
        ////
        chrome.tabs.onCreated.addListener(function(TAB)
        {            
                                                          
            var wait = setInterval(function(){
                if(TAB.status=="complete")
                {
                    var url1 = "";
                    chrome.tabs.query({active:true},function(tabs){
                        url1 = tabs[0].url;
                    });
        
                    if(url1.includes("apklausos.vgtu.lt"))
                    {
                        return;
                    }
                    
                    
                    //sending to dom.js 
                    surveyTab = TAB.id;
                    chrome.tabs.sendMessage(TAB.id,{task:"pressNext"});
                    clearInterval(wait);
                }

            }, 100);          
           
        });   
    }
/////////////////////////////////
/////Once survey has been started
/////////////////////////////////
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, TAB)
{
    if(surveyTab != tabId)
    {
        return;
    }
   
    if(!TAB.url.includes("apklausos.vgtu.lt"))
    {
        return;
    }
   
    if(changeInfo == "loading")
    {
        return;
    }
    
    
    chrome.tabs.sendMessage(tabId,{task:"pildyk"});
    
});

//sending to dom to open survey
function OpenNextSurvey()
{
    
    chrome.tabs.query({url:"https://mano.vgtu.lt/*"},function(tabs)
    {

	    chrome.tabs.reload(tabs[0].id, function(callback)
        {   

            var wait = setInterval(function()
            {
                    chrome.tabs.query({url:"https://mano.vgtu.lt/*"},function(tabs1)
                    {

                    if(tabs1[0].status == "complete")
                    {
                        chrome.tabs.sendMessage(tabs1[0].id,{task:"openSurvey"});
                        clearInterval(wait);
                    }
                    });
            },500);          
        });
    });
    
}

function RemoveLeftOvers()
{
    chrome.tabs.query({url:"http://apklausos.vgtu.lt/*"},function(tabs)
    {
        var ids;
        for(var i = 0;i<tabs.length;i++)
        ids.push(tabs[i].id);
        
        chrome.tabs.remove(ids);
    });
}













//manipulates dom
var settings;
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse)
    {
        switch(request.task)
        {
            case "getLength":
                var count = CountAllSurveys();
                sendResponse({size:count});   
                return true;
                
            case "openSurvey": 
                OpenSurvey();
                return true;
            
            case "pressNext":
                document.getElementById("movenextbtn").click();
                return true;
            case "pildyk":
                Pildyk();
                return true;
       
        }           
    });

function CountAllSurveys()
{
    return document.getElementsByClassName("el_var_block").length;   
}
//opens survey in mano.vgtu main psl
function OpenSurvey()
{
    try
    {
        document.querySelectorAll("a[href*='http://apklausos.vgtu.lt']")[0].click()
    }
    catch(e)
    {
        chrome.runtime.sendMessage({task:"removeLeftovers"});
        return false;
    }
    return true;
}
////////////////////////////////////
//////sending 'startSurvey' to background.js
////////////////////////////////////
function SendMessage(task)
{
    chrome.runtime.sendMessage({task:task},function(response)
    {
        if(response.status=="started")
        {
            OpenSurvey();
        }
        return false;
    });
}

function Pildyk()
{   
    var res;
    var inputs = document.querySelectorAll("input:not([type=hidden])");
    for(var i = 0;i<inputs.length;i++)
    {
        inputs[i].click();
    }
    res = document.getElementById("movenextbtn");
    if(res)
    {
        res.click();
        return;
    }

    res = document.getElementById("movesubmitbtn");
    if(res)
    {
        res.click();
        chrome.runtime.sendMessage({task:"startOver"});
        return;
    }
 
}